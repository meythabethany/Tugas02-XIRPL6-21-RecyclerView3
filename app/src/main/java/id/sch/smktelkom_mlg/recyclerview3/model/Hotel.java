package id.sch.smktelkom_mlg.recyclerview3.model;

import java.io.Serializable;

/**
 * Created by Meytha Bethany Putri on 14/02/2018.
 */

public class Hotel implements Serializable {
    public String judul, deskripsi, detail, lokasi, foto;

    public Hotel(String judul, String deskripsi, String detail, String
            lokasi, String foto) {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.detail = detail;
        this.lokasi = lokasi;
        this.foto = foto;
    }
}
